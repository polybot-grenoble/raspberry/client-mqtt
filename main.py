# Import the library
import tkinter as tk
from tkinter import ttk
from PIL import Image, ImageTk
import XInput as xi

from path import Path

from server import Server

from robot import Robot

from  time import *


# Create an instance of tkinter
win = tk.Tk()

# Window size
win.geometry("1200x900")

myFrame1 = tk.LabelFrame(win)

myFrame1.grid(column = 0, row = 0, sticky="NW")



canvas = tk.Canvas(win, width=1200, height=800)
canvas.grid(column = 0, row = 1)

imageScale = Image.open("table.png").resize((1200, 800))
img = ImageTk.PhotoImage(imageScale)


myFrame = tk.LabelFrame(win)
myFrame.grid(column = 0, row = 2, sticky="NW")



robot0 = Robot(canvas, myFrame, 0, 180, "#eb6228", "#a83e11")
robot1 = Robot(canvas, myFrame, 1, 180, "#7528eb", "#4d11a8")


robotsIP = []


my_path = Path(canvas, None)



canvas.create_image((0, 0), image = img, anchor = "nw")

def mouseEvent(event):
    robot0.input.updateMouse(event)
    robot1.input.updateMouse(event)
    canvas.focus_set()

def addCheckpoint(event):
    x = robot0.input.x
    y = robot0.input.y
    alpha = robot0.input.alpha
    my_path.addCheckpoint(x, y, alpha)

def resetCheckpoints(event):
    my_path.Reset()

    
def spaceKey(event):
    robot0.input.resetAngleMouseMode(event)
    robot1.input.resetAngleMouseMode(event)

def keyboard_left(event):
    robot0.input.leftRotation(event)
    robot1.input.leftRotation(event)

def keyboard_right(event):
    robot0.input.rightRotation(event)
    robot1.input.rightRotation(event)

    

def connect(address):
    print("{} s'est connecté".format(address))
    serveur.clients[address].recvCallback = reception
    print(serveur.clients)
    robotsIP.append(address)
        
def disconnect(address):
    print("{} s'est déconnecté".format(address))
    robotsIP.remove(address)

def reception(address, data):
    #print("{} : {}".format(address, data))
    data_splitted = data.split('\n')
    for d in data_splitted:
        try:
            if(d != ''):
                if(address == robotsIP[0]):
                    if(d == "Done"):
                        if(robot0.input.mode == 5):
                            robot0.sent = False
                            (robot0.targetCheckpoint, robot0.indexCheckpoint) = my_path.getNewCheckpoint(robot0.indexCheckpoint)
                    else:
                        robot0.updateFrom(d)
                elif(address == robotsIP[1]):
                    if(d != "Done"):
                        robot1.updateFrom(d)
        except:
            print("Donnée reçue non valide : {}".format(d))
    


serveur = Server(connect, disconnect)

def startServer():
    serveur.start(ipInput.get(), 8234)
    buttonConnect["state"] = "disabled"


ipInput = tk.Entry(myFrame1)
ipInput.grid(column = 0, row = 0, sticky="NW")

ipInput.insert(0, '192.168.137.1')
buttonConnect = tk.Button(myFrame1, text='Connecter', command=startServer)
buttonConnect.grid(column = 1, row = 0, sticky="NW")






def update_Frame():

    #robot0.update(robot0.input.x, robot0.input.y, robot0.input.alpha)
    #robot1.update(robot1.input.x, robot1.input.y, robot1.input.alpha)

    #robot0.updateFrom("{:.4f}:{:.4f}:{:.4f}\n".format(robot0.input.x, robot0.input.y, robot0.input.alpha))
    #robot1.updateFrom("{:.4f}:{:.4f}:{:.4f}\n".format(robot1.input.x, robot1.input.y, robot1.input.alpha))

    #maj des consignes
    robot0.input.updatePos()
    robot1.input.updatePos()

    #affichage des positions et consignes
    robot0.draw()
    robot1.draw()
    my_path.draw()


    win.after(15, update_Frame)
    #canvas.update()


def sendInstructions():
    #envoi des consignes
    if(len(robotsIP)>0 and robotsIP[0]):
        
        if(robot0.input.mode == 5):
            
            if(not robot0.sent):
                (posx, posy, posA) = robot0.targetCheckpoint
                serveur.clients[robotsIP[0]].WriteData("p;{:.4f};{:.4f};{:.4f}\n".format(posx, posy, posA))
                robot0.sent = True

        elif(robot0.input.mode == 4):
            dx, dy, da= robot0.input.getSpeedFromController()
            serveur.clients[robotsIP[0]].WriteData("vl;{:.4f};{:.4f};{:.4f};{:.4f}\n".format(dx, dy, da, 0))

        else:
           
            posx, posy, posA= robot0.input.getPos()
            #print("X : {:.4f}, Y : {:.4f}, A : {:.4f}\n".format(posx, posy, posA))
            serveur.clients[robotsIP[0]].WriteData("p;{:.4f};{:.4f};{:.4f}\n".format(posx, posy, posA))


    if(len(robotsIP)>1 and robotsIP[1]):
        posx, posy, posA= robot1.input.getPos()
        serveur.clients[robotsIP[1]].WriteData("p;{:.4f};{:.4f};{:.4f}\n".format(posx, posy, posA))
    
    


    win.after(30, request)

def request():
    if(len(robotsIP)>0 and robotsIP[0]):
        #print("X : {:.4f}, Y : {:.4f}, A : {:.4f}\n".format(posx, posy, posA))
            serveur.clients[robotsIP[0]].WriteData("d\n")


    if(len(robotsIP)>1 and robotsIP[1]):
        serveur.clients[robotsIP[1]].WriteData("d\n")

        
    win.after(30, sendInstructions)

#print(xi.get_connected()[0])
canvas.bind('<B1-Motion>', mouseEvent)
canvas.bind('<Button-1>', mouseEvent)
canvas.bind('<space>', spaceKey)
canvas.bind('<a>', keyboard_left)
canvas.bind('<e>', keyboard_right)
canvas.bind('<Return>', addCheckpoint)
canvas.bind('<BackSpace>', resetCheckpoints)
update_Frame()
sendInstructions()
win.mainloop()
for robot in robotsIP:
    thread = serveur.clients[robot].recvThread
    serveur.clients[robot].close()
    thread.join()


serveur.close()