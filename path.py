import math
from input import *

class Path:
    def __init__(self, canvas, table):
        self.canvas = canvas
        self.table = table

        self.checkpoints = []
        self.checkpointsCount = 0
        self.oval = []
        self.arrow = []
        self.texts = []
        self.display = True


    def addCheckpoint(self, x, y, alpha):
        self.checkpoints.append((x, y, alpha))
        self.oval.append(0)
        self.arrow.append(0)
        self.texts.append(0)
        self.checkpointsCount += 1

    def Reset(self):
        self.checkpoints = []
        for i in range(self.checkpointsCount):

            self.canvas.delete(self.oval[i])
            self.canvas.delete(self.arrow[i])
            self.canvas.delete(self.texts[i])
            
        self.oval = []
        self.arrow = []
        self.texts = []
        self.checkpointsCount = 0


    def updateCheckpoint(self, index, x, y, alpha):
        self.checkpoints[index] = (x, y, alpha)


    def getNewCheckpoint(self, index):
        if(index == -1 or index == self.checkpointsCount-1):
            index = 0
        else:
            index +=1
        
        return (self.checkpoints[index], index)

    def draw(self):
        for i in range(self.checkpointsCount):
            self.canvas.delete(self.oval[i])
            self.canvas.delete(self.arrow[i])
            self.canvas.delete(self.texts[i])
            if(self.display):
                (x, y, alpha) = self.checkpoints[i] 
                alpha_disp = -math.radians(alpha)

                (px , py) = posToPix(x, y)
                radius = 40

                width = 3

                self.oval[i] = self.canvas.create_oval(px-radius+width/2, py-radius+width/2, px+radius-width/2, py+radius-width/2, fill = "", outline = "#00ffff", width = width)
                points = [
                px + radius * math.cos(alpha_disp), py + radius*math.sin(alpha_disp), 
                px + radius * math.cos(3*math.pi/4 + alpha_disp), py + radius * math.sin(3*math.pi/4 + alpha_disp),
                px + radius * math.cos(-3*math.pi/4+ alpha_disp), py + radius * math.sin(-3*math.pi/4 + alpha_disp)
                ]
                self.arrow[i] = self.canvas.create_polygon(points, outline='',fill="#00ffff")

                self.texts[i] = self.canvas.create_text(px, py, text=i, fill="black", font=('Helvetica 15 bold'))
            else:
                self.oval[i] = 0
                self.arrow[i] = 0