# remplacée par mqtt



import socket
from threading import Thread




class Server:
    def __init__(self, connectCallback, disconnectCallback):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clients = {}
        self.active = False
        self.connectCallback = connectCallback
        self.disconnectCallback = disconnectCallback
        self.waitThread = Thread(name = "Serveur", target=self.waitForClient)


    def start(self, IP, PORT):
        self.server.bind((IP, PORT))
        self.server.listen(1)
        self.active = True
        self.waitThread.start()
    
    def close(self):
        #self.server.shutdown(socket.SHUT_RDWR)
        self.server.close()
        self.waitThread.join()

    def waitForClient(self):
        while(self.active):
            client, adresseClient = self.server.accept()
            if(self.active):
                self.clients[adresseClient[0]] = ClientManager(self, client, adresseClient[0])
                self.connectCallback(adresseClient[0])
                self.clients[adresseClient[0]].start()
    
    def removeClient(self, address):
        print(self.clients)
        self.clients.pop(address)
        self.disconnectCallback(address)


class ClientManager:
    def __init__(self, server, client, address, recvCallback = None):
        self.server = server
        self.client = client
        self.address = address
        self.recvCallback = recvCallback
        self.connected = True
        self.recvThread = Thread(name = address, target=self.WaitForData)
        

    def close(self):
        self.connected = False
        self.client.shutdown(socket.SHUT_RDWR)
        self.client.close()
        self.server.removeClient(self.address)
    
    def WaitForData(self):
        while(self.connected):
            data = self.client.recv(1024)
            if not data:
                self.connected = False
                self.close()
                break

            self.recvCallback(self.address, data.decode("utf8"))
        
        


    def start(self):
        self.recvThread.start()

    def WriteData(self, data):
        self.client.send(data.encode())

