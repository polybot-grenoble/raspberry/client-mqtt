import math
from input import *

class Robot:
    def __init__(self, canvas, boxSelect, num, robotRadius, colorSet, colorReal):
        self.input = posInput(canvas, boxSelect, num, colorSet, robotRadius)
        self.color = colorReal
        self.canvas = canvas
        self.robotRadius = robotRadius
        self.mode = "BF"
        self.x = 0
        self.y = 0
        self.alpha = 0

        self.oval = 0
        self.line = 0

        self.sent = False
        self.targetCheckpoint = (self.robotRadius+5,self.robotRadius+5,0)
        self.indexCheckpoint = -1


    def updateFrom(self, str):
        args = str.split(';')
        self.x = float(args[0])
        self.y = float(args[1])
        self.alpha = float(args[2])

    def update(self, x, y, alpha):
        self.x = x
        self.y = y
        self.alpha = alpha
        
    def draw(self):
        self.canvas.delete(self.oval)
        self.canvas.delete(self.line)
        if(self.input.mode != 0):
            px , py = posToPix(self.x, self.y)
            radius = mmToPix(self.robotRadius)
            
            alpha_disp = -math.radians(self.alpha)

            width = 10

            self.oval = self.canvas.create_oval(px-radius+width/2, py-radius+width/2, px+radius-width/2, py+radius-width/2, fill = "white", outline = self.color, width = width)
            self.line = self.canvas.create_line(px, py, px + radius * math.cos(alpha_disp), py - radius * math.sin(alpha_disp), width=4, fill="red")


        else:
            self.oval = 0
            self.line = 0
        
        self.input.draw()

            
    def limitCoordinates(self):
        if(self.x > 3000 - self.robotRadius):
            self.x = 3000 - self.robotRadius
        if(self.x < self.robotRadius):
            self.x = self.robotRadius
        if(self.y > 2000 - self.robotRadius):
            self.y = 2000 - self.robotRadius
        if(self.y < self.robotRadius):
            self.y = self.robotRadius
    


    
        

