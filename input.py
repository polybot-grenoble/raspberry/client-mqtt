
import math
import XInput as xi

import tkinter as tk
from tkinter import ttk

def mmToPix(mm):
    return mm * 2/5

def pixTomm(pix):
    return pix * 5/2

def posToPix(x, y):
    px = mmToPix(x)
    py = 800 - mmToPix(y)
    return (px, py)

def pixToPos(px, py):
    x = pixTomm(px)
    y = 2000 - pixTomm(py)
    return (x, y)


class posInput:

    def __init__(self, cv, parent, num, color, robotRadius):
        self.canvas = cv
        self.parent = parent
        self.num = num
        self.color = color
        self.robotRadius = robotRadius
        self.mode = 0
        self.isControllerConnected = 0
        self.controllerIndex = num
        self.mousex = 0
        self.mousey = 900
        self.display = 0

        self.x = self.robotRadius + num*3*self.robotRadius
        self.y = self.robotRadius
        self.alpha = 0
        self.px , self.py = posToPix(self.x, self.y)
        self.oval = 0
        self.line = 0
        self.controllerX = 0
        self.controllerY = 0


        self.modeSelection = ttk.Combobox(parent, state="readonly", 
                            values=[
                                    "Désactivé",
                                    "Souris", 
                                    "Manette Mode 1",
                                    "Manette Mode 2",
                                    "Manette BO",
                                    "Tracé"])
        self.modeSelection.grid(column = num, row = 0)
        self.modeSelection.bind("<<ComboboxSelected>>", self.updateMode)
        self.modeSelection.current(0)

        self.controllerSelection = ttk.Combobox(parent, state="readonly", 
                            values=[
                                    "Manette 1", 
                                    "Manette 2", 
                                    "Manette 3", 
                                    "Manette 4"])
        self.controllerSelection.grid(column = num, row = 1)
        
        self.controllerSelection.bind("<<ComboboxSelected>>", self.updateController)
        self.controllerSelection.current(0)
        
    
    def updateMode(self, event):
        self.mode = self.modeSelection.current()
        if(self.mode == 0 or self.mode == 4 or self.mode == 5): 
            self.display = 0
        else:
            self.display = 1
    
    def updateController(self, event):
        self.controllerIndex = self.controllerSelection.current()
    
    
        
    def resetAngleMouseMode(self, event):
        if(self.mode == 1):
            self.alpha = 0

    def leftRotation(self, event):
        if(self.mode == 1):
            self.alpha += 45
            self.clampAngle()

    
    def rightRotation(self, event):
        if(self.mode == 1):
            self.alpha -= 45
            self.clampAngle()

    def updateMouse(self, event):
        if(self.mode == 1):
            self.mousex = event.x
            self.mousey = event.y


    def updatePos(self):
        match self.mode:
            case 1:
                self.px = self.mousex
                self.py = self.mousey
                self.x, self.y = pixToPos(self.px, self.py)
            case 2:
                if(xi.get_connected()[self.controllerIndex]):
                    state = xi.get_state(self.controllerIndex)
                    (lx, ly), (rx, ry) = xi.get_thumb_values(state)

                    self.x += lx*20
                    self.y += ly*20
                    self.alpha -= rx*4
                    self.clampAngle()

                    (self.px, self.py) = posToPix(self.x, self.y)
            case 3:
                if(xi.get_connected()[self.controllerIndex]):
                    state = xi.get_state(self.controllerIndex)
                    print(xi.get_button_values(state))
                    (lx, ly), (rx, ry) = xi.get_thumb_values(state)

                    self.x += lx*20
                    self.y += ly*20
                    if(rx*rx + ry*ry > 0.1):
                        self.alpha = math.degrees(math.atan2(ry, rx))

                    (self.px, self.py) = posToPix(self.x, self.y)
        
        self.limitCoordinates()


    def getPos(self):
        return (self.x, self.y, self.alpha)
    
    def getSpeedFromController(self):
        if(xi.get_connected()[self.controllerIndex]):
            state = xi.get_state(self.controllerIndex)
            (lx, ly), (rx, ry) = xi.get_thumb_values(state)
            return(lx*500, ly*500, -rx*100)

    def draw(self):
        self.canvas.delete(self.oval)
        self.canvas.delete(self.line)
        if(self.display):
            px , py = posToPix(self.x, self.y)
            radius = mmToPix(self.robotRadius) - 5

            alpha_disp = -math.radians(self.alpha)

            width = 5

            self.oval = self.canvas.create_oval(px-radius+width/2, py-radius+width/2, px+radius-width/2, py+radius-width/2, fill = "white", outline = self.color, width = width)
            self.line = self.canvas.create_line(px, py, px + radius * math.cos(alpha_disp), py - radius * math.sin(alpha_disp), width=4, fill="red")

        else:
            self.oval = 0
            self.line = 0

    def limitCoordinates(self):
        if(self.x > 3000 - self.robotRadius):
            self.x = 3000 - self.robotRadius
        if(self.x < self.robotRadius):
            self.x = self.robotRadius
        if(self.y > 2000 - self.robotRadius):
            self.y = 2000 - self.robotRadius
        if(self.y < self.robotRadius):
            self.y = self.robotRadius

        (self.px, self.py) = posToPix(self.x, self.y)

    def clampAngle(self):
        if(self.alpha > 180):
            self.alpha -= 360
        elif(self.alpha < -180):
            self.alpha += 360
